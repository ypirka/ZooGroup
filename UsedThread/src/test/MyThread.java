package test;

import java.util.Random;

public class MyThread implements Runnable {
    private int max=10+new Random().nextInt(10);
    public MyThread(){

    }
    public MyThread(int max){
        this.max=max;
    }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" Start");
        for (int i = 0; i < max; i++) {
            try {
                Thread.sleep(500);
                System.out.println(Thread.currentThread().getName() + " Tick " + i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + " Finished");
    }
}
