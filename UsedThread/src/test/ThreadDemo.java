package test;

public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Hello from tread - [" + Thread.currentThread().getThreadGroup() + "]:" + Thread.currentThread().getName());
        Thread t1 = new Thread(new MyThread(5));
        t1.start();
        Thread t2 = new Thread(new MyThread(2));
        t2.start();
        Thread t3 = new Thread(new MyThread());
        t3.start();
        if (t1.isAlive()) {
            System.out.println(t1.getName() + " alive. Joining... ");
            t1.join();
        } else {
            System.out.println(t1.getName() + " dead.");
        }
        if (t2.isAlive()) {
            System.out.println(t2.getName() + " alive. Joining... ");
            t2.join();
        } else {
            System.out.println(t2.getName() + " dead.");
        }
        if (t3.isAlive()) {
            System.out.println(t3.getName() + " alive. Joining... ");
            t3.join();
        } else {
            System.out.println(t3.getName() + " dead.");
        }
        System.out.println(Thread.currentThread().getName() + " Finished");
    }

}
