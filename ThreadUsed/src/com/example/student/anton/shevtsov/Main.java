package com.example.student.anton.shevtsov;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Main {
    Set<NewsInstance> newsSet = new HashSet();
    private Object foo = new Object();
    public Main() {
        Thread t = new ConsoleConsumerThread();
        NewsFeed nf = new NewsFeed(new NewsListener() {
            @Override
            public void process(NewsInstance n) {
                synchronized (t) {
                    newsSet.add(n);
                    t.notify();
                }
            }
        });
        t.start();
    }

    public static void main(String[] args) {
        new Main();
    }

    private class ConsoleConsumerThread extends Thread {

        @Override
        public void run() {
            while (true) {
                synchronized (this) {
                    Iterator<NewsInstance> iterator = newsSet.iterator();
                    while (iterator.hasNext()) {
                        NewsInstance item = iterator.next();
                        System.out.println(item);
                        iterator.remove();
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
