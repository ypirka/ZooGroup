package com.example.student.anton.shevtsov;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewsInstance {
    private static DateFormat df= new SimpleDateFormat("dd.MM.yy HH:mm");
    private long timestamp;
    private String text;

    public NewsInstance(String text) {
        this.timestamp = System.currentTimeMillis();
        this.text = text;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {

        return df.format(new Date(timestamp))+" "+ text;

    }
}
