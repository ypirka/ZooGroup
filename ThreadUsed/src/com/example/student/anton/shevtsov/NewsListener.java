package com.example.student.anton.shevtsov;

public interface NewsListener {
    void process (NewsInstance n);
}
