package com.example.student.anton.shevtsov;

import java.util.Random;

public class NewsFeed implements Runnable {
    private Thread thread;
    private NewsListener newsListener;

    public NewsFeed(NewsListener newslistener) {
        this.thread = new Thread(this, "newsFeedThread");
        this.newsListener = newslistener;
        this.thread.start();
    }

    @Override
    public void run() {
        Random r = new Random();

        for (; ; ) {
            NewsInstance ni = new NewsInstance("news №1: bitcoin die" + r.nextInt(6000));
            if (newsListener != null) {
                newsListener.process(ni);
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

