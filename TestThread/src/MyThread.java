import java.util.Random;

public class MyThread extends Thread{

    private int num;

    public MyThread(String name, int num){
        this.setName(name);
        this.num = num;
    }

    @Override
    public void run() {

        for(int i = 0; i<num; i++){
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+" : i="+i);

        }
        System.out.println(Thread.currentThread().getName()+" finished");
    }
}
