public class Main {

    public static void main(String[] args) {

        Thread thread = Thread.currentThread();
        System.out.println("[" + thread
                .getThreadGroup().getName() + "]:" + thread.getName());

        Thread t1 = new MyThread("t1", 10);
        Thread t2 = new MyThread("t2", 2);
        Thread t3 = new MyThread("t3", 5);

            try {
                t1.start();
                synchronized (new Object()) {
                    t1.wait();
                }
                t2.start();
                t2.join();
                t3.start();
                t3.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        System.out.println(thread.getName() + " finished");
    }
}
