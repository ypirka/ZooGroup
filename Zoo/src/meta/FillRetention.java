package meta;

import animals.Animal;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface FillRetention {
    double value() default Animal.INITIAL_FILL;
}
