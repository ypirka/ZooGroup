package lifeAnimals;

import animals.Animal;
import main.ExtensibleCage;

import java.util.Map;
import java.util.Set;

public class CheckThread extends Thread {
    private boolean isRun = true;
    private static CheckThread instance;
    private Map<String, ExtensibleCage<? extends Animal>> cages;

    private CheckThread() {

    }

    public static CheckThread getInstance() {
        synchronized (CheckThread.class) {
            if (instance == null) {
                instance = new CheckThread();
            }
            return instance;
        }
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();

        while (isRun) {
            long now = System.currentTimeMillis();
            if (now >= startTime) {
                startTime = now;
                Set<String> keys = cages.keySet();
                for (String key : keys) {
                    ExtensibleCage<Animal> cage = (ExtensibleCage<Animal>) cages.get(key);
                    for (Animal animal : cage.getCage()) {
                        animal.setFill(animal.getFill() - 1);
                        if (animal.getFill() <= 0) {
                            animal.die();
                        }
                    }
                }
            }
        }

    }

    public void startWork(Map<String, ExtensibleCage<? extends Animal>> cages) {
        setCages(cages);
        start();
    }

    public void stopWork() {
        isRun = false;
    }

    public void setCages(Map<String, ExtensibleCage<? extends Animal>> cages) {
        this.cages = cages;
    }
}